package org.example.service;


import com.example.grpc.service.FileServiceGrpc;
import com.example.grpc.service.FileServiceOuterClass.File;
import com.example.grpc.service.FileServiceOuterClass.FileUploadRequest;
import com.example.grpc.service.FileServiceOuterClass.FileUploadResponse;
import com.example.grpc.service.FileServiceOuterClass.MetaData;
import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.interseptor.GRpcTokenInterceptor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


@Slf4j
@AllArgsConstructor
@Service
public class FileUploadService {

    public static final String PATH_FILE = "src/main/resources/input/gRPC.pdf";

    private final GRpcTokenInterceptor tokenInterceptor;


    @Scheduled(fixedDelay = 12000)
    public void fileUpload() throws IOException {
        final ManagedChannel channel = ManagedChannelBuilder
                .forAddress("localhost", 9090)
                .usePlaintext()
                .intercept(tokenInterceptor)
                .build();

        StreamObserver<FileUploadRequest> streamObserver = FileServiceGrpc.newStub(channel)
                .upload(new FileUploadObserver());

        // input file for testing
        Path path = Paths.get(PATH_FILE);

        // build metadata
        MetaData metadata = MetaData.newBuilder().setName("output").setType("pdf").build();

        FileUploadRequest fileUploadRequest = FileUploadRequest.newBuilder()
                .setMetadata(metadata)
                .build();

        streamObserver.onNext(fileUploadRequest);

        // upload bytes
        InputStream inputStream = Files.newInputStream(path);
        byte[] bytes = new byte[4096];
        int size;
        while ((size = inputStream.read(bytes)) > 0) {
            File file = File.newBuilder()
                    .setContent(ByteString.copyFrom(bytes, 0, size))
                    .build();

            FileUploadRequest uploadRequest = FileUploadRequest.newBuilder()
                    .setFile(file)
                    .build();

            streamObserver.onNext(uploadRequest);
        }

        inputStream.close();

        streamObserver.onCompleted();

        channel.shutdown();
    }

    private static class FileUploadObserver implements StreamObserver<FileUploadResponse> {
        @Override
        public void onNext(FileUploadResponse fileUploadResponse) {
            log.info("File upload status : " + fileUploadResponse.getStatus());
        }

        @Override
        public void onError(Throwable throwable) {

        }

        @Override
        public void onCompleted() {

        }
    }
}
