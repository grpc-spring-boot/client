package org.example.service;

import com.example.grpc.service.UserResponseOuterClass;
import com.example.grpc.service.UserServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.interseptor.GRpcTokenInterceptor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

import static org.example.util.Constant.JWT_METADATA_KEY;


@Slf4j
@AllArgsConstructor
@Service
public class UserServiceImpl {

    private final GRpcTokenInterceptor tokenInterceptor;


    @Scheduled(fixedDelay = 10000)
    public UserResponseOuterClass.UserResponse saveUser() {
        String token = "token";

        Metadata metadata = new Metadata();
        metadata.put(JWT_METADATA_KEY, token);

        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 9090)
                .usePlaintext()
                .keepAliveTime(1, TimeUnit.MINUTES)
                .keepAliveTimeout(5, TimeUnit.SECONDS)
                .keepAliveWithoutCalls(true)
                .intercept(tokenInterceptor)
                .build();

        UserServiceGrpc.UserServiceBlockingStub stub =
                UserServiceGrpc.newBlockingStub(channel);

        UserResponseOuterClass.UserRequest user = UserResponseOuterClass.UserRequest
                .newBuilder()
                .addHobbies("JAVA")
                .setName("Name")
                .setSurname("Surname")
                .build();

        UserResponseOuterClass.UserResponse userResponse = stub.saveUnary(user);
        channel.shutdown();

        return userResponse;
    }
}
